# Основной проект terraform для развертывания k8s

Проект используется в качестве личных экспериментов и сделан просто ради фана.

> На самом деле я просто устал пересобирать
> себе кластер для тестирования каких либо
> сценариев / чартов / приложений.

## Installation

  Устанавливаем необходимые приложения и зависимости
  
  Macbook M1 Fix:
  ```
  brew uninstall terraform
  brew install tfenv
  TFENV_ARCH=amd64 tfenv install 1.4.6
  tfenv use 1.4.6
  ```

  Для работы metallb необходимо переключить strictAPR в значение true

```
kubectl get configmap kube-proxy -n kube-system -o yaml | \
sed -e "s/strictARP: false/strictARP: true/" | \
kubectl diff -f - -n kube-system

kubectl get configmap kube-proxy -n kube-system -o yaml | \
sed -e "s/strictARP: false/strictARP: true/" | \
kubectl apply -f - -n kube-system
```

Пример использования находится в файле main.tf.example
Для более полноценных сценариев и большей кастомизации необходимо читать README.md к используемым плагинам

## Используемые в проекте плагины

| Plugin | README |
| ------ | ------ |
| 00_Main Module | [https://gitlab.com/cephtember/iac_tf_project] |
| 01_CloudInit Module ProxMox| [https://gitlab.com/cephtember/iac_cloudinit_tf_module_proxmox] |
| 01_CloudInit Module Nutanix| [https://gitlab.com/cephtember/iac_cloudinit_tf_module_nutanix] |
| 02_Core Module | [https://gitlab.com/cephtember/iac_core_k8s_tf_module] |
| 03_Security Module | [https://gitlab.com/cephtember/iac_security_k8s_tf_module] |